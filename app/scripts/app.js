'use strict';

/**
 * @ngdoc overview
 * @name mpdclientApp
 * @description
 * # mpdclientApp
 *
 * Main module of the application.
 */
angular
  .module('mpdclientApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.sortable',
    'ngMaterial',
    'ui.router'
  ])
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('index', {
      url: '/',
      views: {
        'index': {
          templateUrl: 'views/main.html',
          controller: 'MainCtrl',
        },
        'status': {
          template: "<div>TEST</div>",
        }
      }
    });
  }])
  .run(function($http, $cookies, $state) {
    $http.defaults.headers.common['X-CSRFToken'] = $cookies.csrftoken;
    $state.go('index');
  });
