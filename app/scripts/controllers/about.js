'use strict';

/**
 * @ngdoc function
 * @name mpdclientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mpdclientApp
 */
angular.module('mpdclientApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
