'use strict';

/**
 * @ngdoc function
 * @name mpdclientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mpdclientApp
 */
angular.module('mpdclientApp')
  .controller('MainCtrl', function($scope, Artist, Status, Utils){
    var artists = Artist.query(function(){
      console.log(artists);
    });

    var artist = Artist.get({ artistId:1 }, function(){
      console.log(artist);
    });

    Status.getStatus(function(data){
      console.log(data);
      $scope.status = data;
    });

    console.log(Utils.toTime(100));

    $scope.artists = artists;

    $scope.rating = 3;

  });
