'use strict';

angular.module('mpdclientApp').factory('Album', function($resource) {
  return $resource('/api/artist/:artistId/album/:albumId', { artistId: '@_artistId' }, {
    query: { isArray:false }
  });
});
