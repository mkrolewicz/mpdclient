'use strict';

angular.module('mpdclientApp').factory('Status', function($http) {
  return {
    getStatus: function(callback) {
      $http.get('/ajax/update_info').success(callback);
    }
  };
});
