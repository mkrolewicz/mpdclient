'use strict';

angular.module('mpdclientApp').factory('Utils', function(){
  return {
    toTime: function(seconds) {
      var secs = Math.floor( seconds % 60 );
      return Math.floor( seconds / 60 ) + ':' + ( ( secs + '').length < 2 ? ('0' + secs ) : secs );
    }
  };
});

