'use strict';

angular.module('mpdclientApp').factory('Artist', function($resource) {
  return $resource('/api/artist/:artistId', {artistId: '@_artistId'});
});
